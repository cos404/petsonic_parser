require 'curb'
require 'nokogiri'
require 'csv'
require 'progress_bar'


### Url category for parse products link
print "Enter category url: "
url = gets.chomp
url = url.sub(/\/\?p=(.*)/, "/") if url.include?("?p=")
raise "Bad url. I working only with: petsonic.com" unless url.include?("petsonic.com")

print "Enter the filename: "
file_name = gets.chomp

### Download and parse URL
http = Curl.get(url)
http_parse = Nokogiri::HTML(http.body_str)

### Parse all pages category
category_parse = []
puts "Parsing pages category."
total_pages = http_parse.xpath('//ul[@class="pagination pull-left"]/li[last()-1]//span/text()').to_s.to_i
category_bar = ProgressBar.new(total_pages, :bar, :rate, :eta)
total_pages.times do |e|
  category = Curl.get(url + "?p=#{e+1}")
  category_parse << Nokogiri::HTML(category.body_str)
  category_bar.increment!
end

### Parse items links
items_links = []
category_parse.length.times do |l|
  category_parse[l].xpath('//a[@class="product-name"]/@href').each do |link|
    items_links << link
  end
end

### Parse products info
items = []
puts "Parsing items info."
item_bar = ProgressBar.new(items_links.length, :bar, :rate, :eta)
items_links.length.times do |l|
  item = Curl.get(items_links[l].to_s)
  item_parse = Nokogiri::HTML(item.body_str)
    img   = item_parse.xpath('//img[@id="bigpic"]/@src').to_s
    name  = item_parse.xpath('//h1[@itemprop="name"]/text()').to_s.delete!("\n").strip
    item_parse.xpath('//ul[@class="attribute_labels_lists"]').each do |e|
      price = e.xpath('child::li/span[@class="attribute_price"]/text()').to_s.delete!("\n").strip
      count = e.xpath('child::li/span[@class="attribute_name"]/text()').to_s
      items << [name + " - " + count, price, img]
    end
    item_bar.increment!
end

### Saving in CSV file
Dir.mkdir("csv") unless File.exists?("csv")
CSV.open("csv/#{file_name}.csv", "wb") do |csv|
  items.each do |item|
    csv << item
  end
end

puts "Complete!"